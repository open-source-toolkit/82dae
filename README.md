# CentOS 7.x SSH 9.3p1 & SSL 1.1.1u 一键升级脚本

---

## 资源简介

针对CentOS 7.x系统的用户，我们提供了这款便捷的升级脚本——`upgrade_ssl_ssh.sh`，旨在简化系统中OpenSSH和OpenSSL的升级流程。此脚本通过RPM包的形式，实现了一键式快速升级，大大节省了管理员的时间，避免了在多台服务器上重复编译安装的工作。升级后，您的系统将拥有最新的安全性更新和性能提升。

- **主要特性**:
    - 同时升级OpenSSH至9.3p1与OpenSSL至1.1.1u。
    - 使用RPM包，一键操作，简化升级步骤。
    - 隐藏OpenSSH版本信息以增强安全性。
    - 自动处理SCP和ssh-copy-id命令，确保功能完整不缺失。

## 安装与验证

### 安装步骤

1. 将本仓库克隆或下载到您的CentOS 7.x服务器上。
2. 进入含有`upgrade_ssl_ssh.sh`的目录。
3. 给予脚本执行权限：`chmod +x upgrade_ssl_ssh.sh`。
4. 执行脚本进行升级：`bash upgrade_ssl_ssh.sh`。

### 验证步骤

升级完成后，您可以通过以下命令来验证是否成功：

- **验证OpenSSH版本**：
    ```shell
    [root@localhost ~]# ssh -V
    OpenSSH_9.3p1 OpenSSL 1.1.1u  30 May 2023
    ```

- **验证OpenSSL版本**：
    ```shell
    [root@localhost ~]# openssl version
    OpenSSL 1.1.1u  30 May 2023
    ```

## 注意事项

- 在执行升级前，强烈建议备份您的系统配置和重要数据。
- 确保服务器具有访问互联网的权限，以便脚本可以下载必要的RPM包。
- 此脚本适用于标准的CentOS 7环境，对于高度定制化的系统可能需要额外的手动调整。

享受安全升级带来的便利，如果您在使用过程中遇到任何问题，欢迎提交 issue 或参与讨论。让我们共同维护您的系统安全与稳定性。